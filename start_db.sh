#!/usr/bin/env bash

docker run --name postgres -e POSTGRES_DB=qr_tracker -ePOSTGRES_PASSWORD=password -ePOSTGRES_HOST_AUTH_METHOD=trust -p5432:5432 --rm postgres