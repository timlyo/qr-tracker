#!/usr/bin/env bash

set -e

cargo build --release

rsync target/release/qr-track timmaddison.co.uk:/tmp
rsync qr-track.service timmaddison.co.uk:/tmp
rsync install.sh timmaddison.co.uk:/tmp
rsync -r sql timmaddison.co.uk:/tmp
rsync flyway.conf timmaddison.co.uk:/tmp

ssh timmaddison.co.uk -t "sudo bash /tmp/install.sh"