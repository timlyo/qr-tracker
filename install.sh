#!/usr/bin/env bash

set -e

systemctl stop qr-track

cp /tmp/qr-track /usr/bin/qr-track
cp /tmp/qr-track.service /etc/systemd/system/qr-track.service

(
  cd /tmp && flyway migrate
)

systemctl daemon-reload
systemctl start qr-track
systemctl enable qr-track
