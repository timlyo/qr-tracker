//! Container for more complex business logic
use sqlx::types::Uuid;

use crate::database;
use crate::database::Pool;
use crate::models::codes::NewCode;
use crate::models::timelines::NewTimeline;
use crate::models::users::NewUser;
use sqlx::Error;
use std::fmt::Display;

pub enum SystemError {
    AlreadyExists(Box<dyn Display>),
    DatabaseError(sqlx::Error),
}

impl From<sqlx::Error> for SystemError {
    fn from(e: Error) -> Self {
        SystemError::DatabaseError(e)
    }
}

pub async fn create_new_user(db_pool: &Pool, new_user: NewUser<'_>) -> Result<Uuid, SystemError> {
    if database::users::exists(db_pool, &new_user).await? {
        return Err(SystemError::AlreadyExists(Box::new(String::from(
            new_user.email,
        ))));
    }

    let user_id = database::users::insert_user(db_pool, new_user).await?;
    let timeline_id = database::timelines::insert_timeline(
        db_pool,
        NewTimeline {
            owner: user_id,
            description: String::from("Default timeline"),
        },
    )
    .await?;
    database::codes::insert_new(
        db_pool,
        NewCode {
            description: "Default code".to_string(),
            user_id,
            initial_timeline: timeline_id,
        },
    )
    .await?;

    Ok(user_id)
}
