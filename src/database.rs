use crate::models;
use sqlx::Pool as SqlxPool;
use sqlx::{types::Uuid, PgConnection, PgPool};
use std::env;

pub use sqlx::Result;

pub type Pool = SqlxPool<PgConnection>;

pub async fn create_pool() -> Result<SqlxPool<PgConnection>> {
    let address = env::var("DATABASE_URL").unwrap_or(String::from(
        "postgresql://postgres@localhost:5432/qr_tracker",
    ));

    PgPool::builder().max_size(16).build(&address).await
}

pub mod codes {
    use super::*;

    pub async fn insert_new(pool: &Pool, code: models::codes::NewCode) -> sqlx::Result<Uuid> {
        sqlx::query!(
            r#"INSERT INTO codes (description, user_id, current_timeline)
            VALUES ($1, $2, $3)
            RETURNING code_id
            "#,
            code.description,
            code.user_id,
            code.initial_timeline
        )
        .fetch_one(pool)
        .await
        .map(|record| record.code_id)
    }

    pub async fn get_for_timeline(
        pool: &Pool,
        timeline_id: Uuid,
    ) -> sqlx::Result<Vec<models::codes::Code>> {
        sqlx::query_as!(
            models::codes::Code,
            r#"SELECT * FROM codes WHERE current_timeline = $1"#,
            timeline_id
        )
        .fetch_all(pool)
        .await
    }

    pub async fn count_for_timeline(pool: &Pool, timeline_id: Uuid) -> sqlx::Result<i64> {
        sqlx::query!(
            r#"SELECT COUNT(*) FROM codes WHERE current_timeline = $1"#,
            timeline_id
        )
        .fetch_one(pool)
        .await
        .map(|row| row.count.unwrap())
    }

    pub async fn get_by_id(pool: &Pool, id: Uuid) -> sqlx::Result<models::codes::Code> {
        sqlx::query_as!(
            models::codes::Code,
            r#"SELECT * FROM codes WHERE code_id = $1"#,
            id
        )
        .fetch_one(pool)
        .await
    }

    pub async fn get_codes_for_user(
        pool: &Pool,
        user_id: Uuid,
    ) -> sqlx::Result<Vec<models::codes::Code>> {
        sqlx::query_as!(
            models::codes::Code,
            r#"
            SELECT * FROM codes WHERE user_id = $1
            ORDER BY created DESC
            "#,
            user_id
        )
        .fetch_all(pool)
        .await
    }

    pub async fn update(
        pool: &Pool,
        code_id: Uuid,
        description: &str,
        current_timeline: Uuid,
    ) -> Result<u64> {
        sqlx::query!(
            r#"UPDATE codes SET description = $2, current_timeline = $3
        WHERE code_id = $1"#,
            dbg!(code_id),
            dbg!(description),
            dbg!(current_timeline)
        )
        .execute(pool)
        .await
    }
}

pub mod events {
    use super::*;
    use chrono::NaiveDateTime;

    pub async fn insert_new(pool: &Pool, event: models::events::NewEvent) -> sqlx::Result<Uuid> {
        sqlx::query!(
            r#"INSERT INTO events (code_id, timeline_id)
            VALUES ($1, $2)
            RETURNING event_id
            "#,
            event.code_id,
            event.timeline_id
        )
        .fetch_one(pool)
        .await
        .map(|record| record.event_id)
    }

    pub async fn count_for_timeline(pool: &Pool, timeline_id: Uuid) -> sqlx::Result<i64> {
        sqlx::query!(
            r#"SELECT COUNT(*) FROM events WHERE timeline_id = $1"#,
            timeline_id
        )
        .fetch_one(pool)
        .await
        .map(|row| row.count.unwrap())
    }

    pub async fn get_for_timeline_since(
        pool: &Pool,
        timeline_id: Uuid,
        since: NaiveDateTime,
    ) -> sqlx::Result<Vec<models::events::Event>> {
        sqlx::query_as!(
            models::events::Event,
            r#"SELECT * FROM events WHERE timeline_id = $1 AND time > $2"#,
            timeline_id,
            since
        )
        .fetch_all(pool)
        .await
    }

    pub async fn get_for_timeline(
        pool: &Pool,
        timeline_id: Uuid,
    ) -> sqlx::Result<Vec<models::events::Event>> {
        sqlx::query_as!(
            models::events::Event,
            r#"SELECT * FROM events WHERE timeline_id = $1"#,
            timeline_id
        )
        .fetch_all(pool)
        .await
    }

    pub async fn get_all(pool: &Pool) -> sqlx::Result<Vec<models::events::Event>> {
        sqlx::query_as!(models::events::Event, r#"SELECT * FROM events"#)
            .fetch_all(pool)
            .await
    }

    pub async fn get_all_for_code(
        pool: &Pool,
        code: Uuid,
    ) -> sqlx::Result<Vec<models::events::Event>> {
        sqlx::query_as!(
            models::events::Event,
            r#"SELECT * FROM events WHERE code_id = $1"#,
            code
        )
        .fetch_all(pool)
        .await
    }
}

pub mod users {
    use super::*;

    pub async fn exists(pool: &Pool, user: &models::users::NewUser<'_>) -> sqlx::Result<bool> {
        sqlx::query!("SELECT COUNT(*) from users WHERE email = $1", user.email)
            .fetch_one(pool)
            .await
            .map(|row| row.count.unwrap() > 0)
    }

    pub async fn insert_user(pool: &Pool, user: models::users::NewUser<'_>) -> sqlx::Result<Uuid> {
        sqlx::query!(
            r#"
            INSERT INTO users (email, password)
            VALUES ($1, crypt($2, gen_salt('bf')))
            RETURNING user_id
            "#,
            user.email,
            user.password
        )
        .fetch_one(pool)
        .await
        .map(|row| row.user_id)
    }

    pub async fn check_password(
        pool: &Pool,
        email: &str,
        password: &str,
    ) -> sqlx::Result<models::users::User> {
        sqlx::query_as!(
            models::users::User,
            r#"SELECT user_id, email FROM users
            WHERE email = $1 AND password = crypt($2, password)
            "#,
            email,
            password
        )
        .fetch_one(pool)
        .await
    }

    pub async fn get_by_id(pool: &Pool, id: Uuid) -> sqlx::Result<models::users::User> {
        sqlx::query_as!(
            models::users::User,
            r#"SELECT user_id, email FROM users
            WHERE user_id = $1
            "#,
            id
        )
        .fetch_one(pool)
        .await
    }
}

pub mod timelines {
    use super::*;
    use crate::models::timelines::TimelineSummary;

    pub async fn get_by_id(pool: &Pool, id: Uuid) -> Result<models::timelines::Timeline> {
        sqlx::query_as!(
            models::timelines::Timeline,
            r#"SELECT * FROM timelines where timeline_id = $1"#,
            id
        )
        .fetch_one(pool)
        .await
    }

    pub async fn get_summary(pool: &Pool, id: Uuid) -> Result<TimelineSummary> {
        let event_count = events::count_for_timeline(pool, id).await?;
        let code_count = codes::count_for_timeline(pool, id).await?;
        let timeline = timelines::get_by_id(pool, id).await?;

        Ok(timeline.to_summary(code_count, event_count))
    }

    pub async fn insert_timeline(
        pool: &Pool,
        timeline: models::timelines::NewTimeline,
    ) -> sqlx::Result<Uuid> {
        sqlx::query!(
            r#"INSERT INTO timelines (description, owner)
            VALUES ($1, $2)
            RETURNING timeline_id
            "#,
            timeline.description,
            timeline.owner
        )
        .fetch_one(pool)
        .await
        .map(|record| record.timeline_id)
    }

    pub async fn set_deleted(pool: &Pool, timeline_id: Uuid) -> sqlx::Result<u64> {
        sqlx::query!(
            r#"UPDATE timelines SET deleted = true WHERE timeline_id = $1"#,
            timeline_id
        )
        .execute(pool)
        .await
    }

    pub async fn get_for_user(
        pool: &Pool,
        user_id: Uuid,
        show_deleted: bool,
    ) -> Result<Vec<models::timelines::Timeline>> {
        sqlx::query_as!(
            models::timelines::Timeline,
            r#"SELECT * FROM timelines
            WHERE owner = $1 AND ($2 OR deleted = false)
            ORDER BY timeline_id"#,
            user_id,
            show_deleted
        )
        .fetch_all(pool)
        .await
    }

    pub async fn get_summaries_for_user(
        pool: &Pool,
        user_id: Uuid,
        show_deleted: bool,
    ) -> Result<Vec<Result<models::timelines::TimelineSummary>>> {
        let timelines: Vec<models::timelines::Timeline> = sqlx::query_as!(
            models::timelines::Timeline,
            r#"SELECT * FROM timelines
                    WHERE owner = $1 AND ($2 or deleted = false)
                    ORDER BY timeline_id"#,
            user_id,
            show_deleted
        )
        .fetch_all(pool)
        .await?;

        Ok(
            futures::future::join_all(timelines.into_iter().map(async move |timeline| {
                let code_count = super::codes::count_for_timeline(pool, timeline.timeline_id).await;
                let event_count =
                    super::events::count_for_timeline(pool, timeline.timeline_id).await;

                match (code_count, event_count) {
                    (Ok(cc), Ok(ec)) => Ok(timeline.to_summary(cc, ec)),
                    (Err(e), _) => Err(e),
                    (_, Err(e)) => Err(e),
                }
            }))
            .await,
        )
    }

    pub async fn update_description(pool: &Pool, id: Uuid, description: &str) -> Result<u64> {
        sqlx::query!(
            r#"UPDATE timelines SET description = $2 WHERE timeline_id = $1"#,
            id,
            description
        )
        .execute(pool)
        .await
    }
}
