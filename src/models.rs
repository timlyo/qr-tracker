use async_std::task;
use sqlx::types::Uuid;

pub mod codes {
    use super::*;

    pub struct Code {
        pub code_id: Uuid,
        /// Free text field for users to describe their code
        pub description: String,
        /// Time code was created
        pub created: chrono::NaiveDateTime,
        /// User who created the code
        pub user_id: Uuid,
        /// Timeline that events will be pushed to
        pub current_timeline: Uuid,
    }

    pub struct NewCode {
        pub description: String,
        pub user_id: Uuid,
        pub initial_timeline: Uuid,
    }
}

pub mod events {
    use super::*;

    #[derive(Debug)]
    pub struct Event {
        pub event_id: Uuid,
        pub time: chrono::NaiveDateTime,
        pub code_id: Uuid,
        pub timeline_id: Uuid,
    }

    pub struct NewEvent {
        pub timeline_id: Uuid,
        pub code_id: Uuid,
    }
}

pub mod users {
    use super::*;
    use crate::database::Pool;
    use crate::{database, forms};
    use rocket::http::Status;
    use rocket::request::FromRequest;
    use rocket::Outcome;
    use rocket::{request, Request, State};

    pub struct User {
        pub user_id: Uuid,
        pub email: String,
    }

    pub struct NewUser<'a> {
        pub email: &'a str,
        pub password: &'a str,
    }

    impl User {
        pub fn profile_url(&self) -> String {
            format!("/user/{}", self.user_id)
        }
    }

    impl<'a> NewUser<'a> {
        pub fn from_form(form: &'a forms::NewUserForm) -> Result<NewUser<'a>, &'static str> {
            if form.password.chars().count() < 8 {
                return Err("Password must be more than 8 characters");
            }

            Ok(NewUser {
                email: &form.email,
                password: &form.password,
            })
        }
    }

    impl<'a, 'r> FromRequest<'a, 'r> for User {
        type Error = sqlx::Error;

        fn from_request(request: &'a Request<'r>) -> request::Outcome<User, Self::Error> {
            let id_request = request
                .cookies()
                .get_private("user_id")
                .and_then(|id| id.value().parse::<Uuid>().ok());

            let id = match id_request {
                None => return Outcome::Forward(()),
                Some(id) => id,
            };

            let pool = request
                .guard::<State<Pool>>()
                .expect("Failed to get database pool");

            match task::block_on(database::users::get_by_id(&pool, id)) {
                Ok(user) => Outcome::Success(user),
                Err(e) => Outcome::Failure((Status::InternalServerError, e)),
            }
        }
    }
}

pub mod timelines {
    use sqlx::types::Uuid;

    #[derive(Debug)]
    pub struct TimelineSummary {
        pub timeline_id: Uuid,
        pub description: String,
        pub owner: Uuid,
        pub deleted: bool,
        pub code_count: i64,
        pub event_count: i64,
    }

    #[derive(Clone, Debug, PartialEq, Default)]
    pub struct Timeline {
        pub timeline_id: Uuid,
        pub description: String,
        pub owner: Uuid,
        pub deleted: bool,
    }

    #[derive(Debug)]
    pub struct NewTimeline {
        pub description: String,
        pub owner: Uuid,
    }

    impl Timeline {
        pub fn to_summary(self, code_count: i64, event_count: i64) -> TimelineSummary {
            TimelineSummary {
                timeline_id: self.timeline_id,
                description: self.description,
                owner: self.owner,
                deleted: self.deleted,
                code_count,
                event_count,
            }
        }
    }
}
