#![feature(proc_macro_hygiene, decl_macro)]
#![feature(async_closure)]

pub mod database;
pub mod filters;
pub mod forms;
pub mod interface;
pub mod models;
pub mod system;
pub mod templates;

use async_std::task;
use std::env;

async fn run() -> database::Result<()> {
    let pool = database::create_pool()
        .await
        .expect("Failed to create database pool");

    interface::start(pool);

    Ok(())
}

fn setup_logger() {
    use simplelog::*;

    let log_level = env::var("LOG_LEVEL")
        .unwrap_or("INFO".to_string())
        .parse()
        .expect("Failed to parse log level env variable");

    TermLogger::init(log_level, Config::default(), TerminalMode::Mixed)
        .expect("Failed to init logger");
}

fn main() {
    setup_logger();
    log::warn!("Starting");

    if let Err(e) = task::block_on(run()) {
        dbg!(e);
    }
}
