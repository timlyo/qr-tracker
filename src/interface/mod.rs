use async_std::task;
use qrcodegen::QrCodeEcc;
use rocket::config::Environment;
use rocket::http::ContentType;
use rocket::logger::LoggingLevel;
use rocket::{get, routes, Response, Route, State};
use rocket_contrib::serve::StaticFiles;
use std::env;
use std::io::Cursor;

use crate::database::Pool;
use crate::models::users::User;
use crate::{database, models, templates};

mod code;
mod common;
mod errors;
mod timeline;
mod user;

use self::common::parse_uuid;
pub use self::common::FlashWrapper;

pub fn start(db_pool: Pool) {
    let mut config = rocket::Config::build(Environment::Development)
        .log_level(LoggingLevel::Normal)
        .port(19000);

    if let Ok(secret_key) = env::var("SECRET_KEY") {
        config = config.secret_key(secret_key);
    }

    rocket::custom(config.finalize().expect("Failed to finalize config"))
        .mount("/", get_routes())
        .mount("/assets", StaticFiles::from("assets"))
        .manage(db_pool)
        .launch();
}

fn get_routes() -> Vec<Route> {
    routes![
        index,
        code::get,
        code::post,
        code::edit,
        generate_qr_image,
        timeline::get,
        timeline::post,
        timeline::post_with_id,
        timeline::delete,
        timeline::delete_confirm,
        trigger_get,
        user::login_get,
        user::login_post,
        user::logout,
        user::newuser_get,
        user::newuser_post,
        user::user_get,
        user::user_get_no_user,
    ]
}

#[get("/")]
pub fn index(
    db_pool: State<Pool>,
    current_user: Option<User>,
    flash: Option<FlashWrapper>,
) -> Result<templates::IndexTemplate, String> {
    let events = task::block_on(database::events::get_all(&db_pool))
        .map_err(|e| format!("Failed to load events from database {:?}", e))?;

    Ok(templates::IndexTemplate {
        events,
        current_user,
        flash,
    })
}

#[get("/trigger/<code_id>")]
pub fn trigger_get(code_id: String, db_pool: State<Pool>) -> Result<String, String> {
    let code_id = parse_uuid(&code_id)?;
    let code: models::codes::Code =
        task::block_on(database::codes::get_by_id(&db_pool, code_id))
            .map_err(|e| format!("Failed to load code from database {:?}", e))?;

    let event_id = task::block_on(database::events::insert_new(
        &db_pool,
        models::events::NewEvent {
            timeline_id: code.current_timeline,
            code_id,
        },
    ))
    .map_err(|e| format!("Failed to save event in database {:?}", e))?;

    Ok(format!("Added event {}", event_id.to_string()))
}

#[get("/qr/<code_id>")]
pub fn generate_qr_image<'a>(code_id: String) -> Result<Response<'a>, qrcodegen::DataTooLong> {
    // TODO add hostname to qr code
    let data = format!("/trigger/{}", code_id);
    let border = 2;
    let image = qrcodegen::QrCode::encode_text(&data, QrCodeEcc::Low)?.to_svg_string(border);
    let image_cursor = Cursor::new(image.into_bytes());

    Ok(Response::build()
        .header(ContentType::SVG)
        .sized_body(image_cursor)
        .finalize())
}
