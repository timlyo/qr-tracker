use crate::interface::common::*;
use crate::interface::errors::{InterfaceError, InterfaceResult};

#[get("/code/<code_id>")]
pub fn get(
    code_id: String,
    db_pool: State<Pool>,
    current_user: models::users::User,
    flash: Option<FlashWrapper>,
) -> InterfaceResult<templates::CodeTemplate> {
    let code_id = Uuid::from_str(&code_id)?;
    let code = task::block_on(database::codes::get_by_id(&db_pool, code_id))?;
    let events = task::block_on(database::events::get_all_for_code(&db_pool, code_id))?;
    let timelines: Vec<_> = task::block_on(database::timelines::get_for_user(
        &db_pool,
        current_user.user_id,
        false,
    ))?;

    let current_timeline = timelines
        .iter()
        .cloned()
        .find(|timeline| timeline.timeline_id == code.current_timeline);

    Ok(templates::CodeTemplate {
        code,
        events,
        timelines,
        current_timeline,
        current_user: Some(current_user),
        flash,
    })
}

/// Edits the details of a code
///
/// Note relies on Rocket's Form request guard to be differentiated from the other post in this module
#[post("/code/edit/<code_id>", data = "<form>")]
pub fn edit(
    db_pool: State<Pool>,
    current_user: models::users::User,
    code_id: String,
    form: Form<forms::CodeEditForm>,
) -> InterfaceResult<Redirect> {
    let code_id = Uuid::from_str(&code_id)?;
    let code: models::codes::Code = task::block_on(database::codes::get_by_id(&db_pool, code_id))?;

    if code.user_id != current_user.user_id {
        return Err(InterfaceError::Access(
            "Cannot edit another user's code".to_string(),
        ));
    }

    let new_timeline = Uuid::from_str(&form.timeline)?;

    let result = task::block_on(database::codes::update(
        &db_pool,
        code_id,
        &form.description,
        new_timeline,
    ))?;

    Ok(Redirect::to(uri!(get: code_id.to_string())))
}

/// Creates a new code assigned to the referenced timeline
#[post("/code/<timeline_id>")]
pub fn post(
    db_pool: State<Pool>,
    current_user: models::users::User,
    timeline_id: String,
) -> InterfaceResult<Flash<Redirect>> {
    let timeline_id = Uuid::from_str(&timeline_id)?;
    let code = models::codes::NewCode {
        description: String::from("New Code"),
        user_id: current_user.user_id,
        initial_timeline: timeline_id,
    };

    let id: Uuid = task::block_on(database::codes::insert_new(&db_pool, code))?;

    Ok(Flash::success(
        Redirect::to(uri!(get: id.to_string())),
        "Created new code",
    ))
}
