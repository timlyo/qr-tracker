use crate::interface::common::*;

#[post("/login", data = "<form>")]
pub fn login_post(
    db_pool: State<Pool>,
    form: Form<forms::LoginForm>,
    mut cookie_jar: Cookies<'_>,
) -> Flash<Redirect> {
    let user = task::block_on(database::users::check_password(
        &db_pool,
        &form.email,
        &form.password,
    ));

    if let Ok(user) = user {
        login_user(user.user_id, &mut cookie_jar);
        Flash::success(Redirect::to(format!("/user/{}", user.user_id)), "Logged in")
    } else {
        Flash::error(Redirect::to("/login"), "Incorrect Login")
    }
}

#[get("/login")]
pub fn login_get(
    current_user: Option<models::users::User>,
    flash: Option<FlashWrapper>,
) -> templates::LoginTemplate {
    templates::LoginTemplate {
        current_user,
        flash,
    }
}

#[post("/logout")]
pub fn logout(mut cookie_jar: Cookies<'_>) -> Flash<Redirect> {
    logout_current_user(&mut cookie_jar);
    Flash::success(Redirect::to("/"), "Logged Out")
}

#[get("/newuser")]
pub fn newuser_get(
    current_user: Option<models::users::User>,
    flash: Option<FlashWrapper>,
) -> templates::NewUserTemplate {
    templates::NewUserTemplate {
        current_user,
        flash,
    }
}

#[post("/newuser", data = "<form>")]
pub fn newuser_post(
    db_pool: State<Pool>,
    form: Form<forms::NewUserForm>,
    mut cookie_jar: Cookies<'_>,
) -> Flash<Redirect> {
    let new_user = match models::users::NewUser::from_form(&form) {
        Ok(user) => user,
        Err(e) => return Flash::error(Redirect::to("/newuser"), e),
    };

    let result = task::block_on(system::create_new_user(&db_pool, new_user));

    let format_error = |error: system::SystemError| match error {
        system::SystemError::AlreadyExists(data) => format!("{} already exists", data),
        system::SystemError::DatabaseError(error) => format!("User creation error {:?}", error),
    };

    match result {
        Ok(id) => {
            login_user(id, &mut cookie_jar);
            Flash::success(Redirect::to(format!("/user/{}", id)), "Account Created")
        }
        Err(e) => Flash::error(Redirect::to("/newuser"), format_error(e)),
    }
}

#[get("/user/<user_id>?<show_deleted>", rank = 1)]
pub fn user_get(
    user_id: String,
    current_user: models::users::User,
    flash: Option<FlashWrapper>,
    db_pool: State<Pool>,
    show_deleted: Option<bool>,
) -> InterfaceResult<templates::UserTemplate> {
    let show_deleted = show_deleted.unwrap_or(false);
    let user_id = Uuid::from_str(&user_id)?;
    let focused_user = task::block_on(database::users::get_by_id(&db_pool, user_id))?;
    let user_codes = task::block_on(database::codes::get_codes_for_user(&db_pool, user_id))?;
    let timelines = task::block_on(database::timelines::get_summaries_for_user(
        &db_pool,
        user_id,
        show_deleted,
    ))?;

    Ok(templates::UserTemplate {
        focused_user,
        user_codes,
        current_user: Some(current_user),
        timelines,
        flash,
    })
}

#[get("/user/<user_id>", rank = 2)]
pub fn user_get_no_user(
    user_id: String,
    current_user: Option<models::users::User>,
) -> Flash<Redirect> {
    Flash::error(
        Redirect::to("/login"),
        "Error must be logged in to view user page",
    )
}
