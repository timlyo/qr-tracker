use crate::interface::common::Status;
use crate::templates::ErrorTemplate;
use rocket::http::ContentType;
use rocket::{response, response::Responder, Request, Response};
use std::io::Cursor;

pub type InterfaceResult<T> = std::result::Result<T, InterfaceError>;

#[derive(Debug)]
pub enum InterfaceError {
    Database(sqlx::Error),
    Data(String),
    Access(String),
}

impl<'r> Responder<'r> for InterfaceError {
    fn respond_to(self, _: &Request) -> response::Result<'r> {
        let (code, message) = match self {
            InterfaceError::Database(e) => (500, format!("Database Error {:?}", e)),
            InterfaceError::Data(e) => (422, e),
            InterfaceError::Access(e) => (401, e),
        };

        log::error!("Error {} {}", code, message);

        let body = ErrorTemplate::render(code, message).expect("Error rendering error template");

        Response::build()
            .header(ContentType::HTML)
            .status(Status::from_code(code).expect("Wrong http code"))
            .sized_body(Cursor::new(body))
            .ok()
    }
}

impl From<sqlx::Error> for InterfaceError {
    fn from(e: sqlx::Error) -> Self {
        InterfaceError::Database(e)
    }
}

impl From<uuid::Error> for InterfaceError {
    fn from(e: uuid::Error) -> Self {
        InterfaceError::Data(format!("Problem parsing id: {}", e.to_string()))
    }
}
