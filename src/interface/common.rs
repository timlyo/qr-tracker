//! Rexports and functions used in most routes

pub use crate::database::Pool;
pub use crate::interface::errors::{InterfaceError, InterfaceResult};
pub use crate::{database, forms, models, system, templates};
pub use futures_util::TryFutureExt;

pub use rocket::http::{Cookie, Cookies, Status};
pub use rocket::request::{FlashMessage, Form, FromRequest};
pub use rocket::response::{status, Flash, Redirect};
use rocket::Request;
pub use rocket::{delete, get, post, uri, State};

pub use async_std::task;
pub use sqlx::types::Uuid;
pub use std::str::FromStr;

pub fn parse_uuid(uuid: &str) -> Result<Uuid, String> {
    Uuid::from_str(&uuid).map_err(|e| format!("Failed to parse uuid {}, {:?}", uuid, e))
}

pub fn login_user(user_id: Uuid, cookie_jar: &mut Cookies<'_>) {
    cookie_jar.add_private(Cookie::new("user_id", user_id.to_string()));
}

pub fn logout_current_user(cookie_jar: &mut Cookies<'_>) {
    cookie_jar.remove_private(Cookie::named("user_id"));
}

/// Wrapper around rocket's Flash that takes ownership of data
pub struct FlashWrapper {
    pub name: String,
    pub message: String,
}

impl FlashWrapper {
    pub fn from_rocket_flash(flash: FlashMessage) -> FlashWrapper {
        FlashWrapper {
            name: String::from(flash.name()),
            message: String::from(flash.msg()),
        }
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for FlashWrapper {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> rocket::request::Outcome<Self, Self::Error> {
        request
            .guard::<rocket::request::FlashMessage>()
            .map(|flash| FlashWrapper::from_rocket_flash(flash))
    }
}
