use crate::interface::common::*;

#[get("/timeline/<timeline_id>")]
pub fn get(
    timeline_id: String,
    current_user: models::users::User,
    db_pool: State<Pool>,
    flash: Option<FlashWrapper>,
) -> InterfaceResult<templates::TimelineTemplate> {
    let timeline_id = Uuid::from_str(&timeline_id)?;
    let timeline = task::block_on(database::timelines::get_summary(&db_pool, timeline_id))?;

    if timeline.owner != current_user.user_id {
        return Err(InterfaceError::Access(
            "Cannot view another user's timeline".to_string(),
        ));
    }

    let codes = task::block_on(database::codes::get_for_timeline(&db_pool, timeline_id))?;
    let events = task::block_on(database::events::get_for_timeline(&db_pool, timeline_id))?;

    Ok(templates::TimelineTemplate {
        current_user: Some(current_user),
        timeline,
        codes,
        events,
        flash,
    })
}

/// Updates a specific timeline. Note should ideally be a PUT, but html forms don't support that
#[post("/timeline/<timeline_id>", data = "<form>")]
pub fn post_with_id(
    timeline_id: String,
    current_user: models::users::User,
    db_pool: State<Pool>,
    form: Form<forms::TimelineForm>,
) -> InterfaceResult<Redirect> {
    let timeline_id = Uuid::from_str(&timeline_id)?;
    let timeline = task::block_on(database::timelines::get_by_id(&db_pool, timeline_id))?;

    if timeline.owner != current_user.user_id {
        return Err(InterfaceError::Access(
            "Cannot edit another user's timeline".to_string(),
        ));
    }

    let result = task::block_on(database::timelines::update_description(
        &db_pool,
        timeline_id,
        &form.description,
    ))?;

    Ok(Redirect::to(uri!(get: timeline_id.to_string())))
}

#[post("/timeline?<from>")]
pub fn post(
    current_user: models::users::User,
    db_pool: State<Pool>,
    from: Option<String>,
) -> Flash<Redirect> {
    let new_timeline = models::timelines::NewTimeline {
        description: "New Timeline".to_string(),
        owner: current_user.user_id,
    };

    let result = task::block_on(database::timelines::insert_timeline(&db_pool, new_timeline))
        .map_err(|e| format!("Failed to insert timeline {:?}", e));

    let origin = from.unwrap_or("/".to_string());
    match result {
        Ok(_) => Flash::success(Redirect::to(origin), "Created new timeline"),
        Err(e) => Flash::error(
            Redirect::to(origin),
            format!("Failed to create timeline: {:?}", e),
        ),
    }
}

#[get("/timeline/<timeline_id>/delete")]
pub fn delete_confirm(
    timeline_id: String,
    current_user: models::users::User,
    flash: Option<FlashWrapper>,
) -> templates::ConfirmTemplate {
    templates::ConfirmTemplate {
        current_user: Some(current_user),
        flash,
        question: "Are you sure you want to delete this timeline?".to_string(),
        yes_location: uri!(delete: timeline_id).to_string(),
        no_location: "/".to_string(),
    }
}

#[post("/timeline/<timeline_id>/delete")]
pub fn delete(
    current_user: models::users::User,
    db_pool: State<Pool>,
    timeline_id: String,
) -> InterfaceResult<Flash<Redirect>> {
    let timeline_id = Uuid::from_str(&timeline_id)?;
    let timeline: models::timelines::Timeline =
        task::block_on(database::timelines::get_by_id(&db_pool, timeline_id))?;

    if timeline.owner != current_user.user_id {
        return Err(InterfaceError::Access(
            "Cannot delete another user's timeline".to_string(),
        ));
    }

    let result: u64 = task::block_on(database::timelines::set_deleted(&db_pool, timeline_id))?;

    Ok(Flash::success(
        Redirect::to(current_user.profile_url()),
        "Deleted timeline. Go to profile to restore",
    ))
}
