//! Askama filters
//! https://docs.rs/askama/0.9.0/askama/index.html#filters

use askama::Error;
use chrono::Datelike;

pub fn human_date(date: &chrono::NaiveDateTime) -> Result<String, Error> {
    let current_time = chrono::offset::Utc::now();
    let format = if date.year() == current_time.year() {
        "%d %B %I:%M"
    } else {
        "%d %B %G %I:%M"
    };

    Ok(date.format(format).to_string())
}

/// Convert a datetime to an iso 8601 formatted string. Required because NaiveDateTime doesn't add the
/// UTC timezone by default
pub fn iso_date(date: &chrono::NaiveDateTime) -> Result<String, Error> {
    Ok(date.format("%Y-%m-%d %H:%M:%S%.fZ").to_string())
}
