use askama::Template;
use sqlx::types::Uuid;

use crate::interface::FlashWrapper;
use crate::models;
use crate::{database, filters};

#[derive(Template)]
#[template(path = "error.html")]
pub struct ErrorTemplate {
    pub code: u16,
    pub message: String,
    pub current_user: Option<models::users::User>,
    pub flash: Option<FlashWrapper>,
}

impl ErrorTemplate {
    pub fn render(code: u16, message: String) -> askama::Result<String> {
        ErrorTemplate {
            code,
            message,
            flash: None,
            current_user: None,
        }
        .render()
    }
}

#[derive(Template)]
#[template(path = "event.html")]
pub struct EventTemplate {
    pub event_id: Uuid,
    pub current_user: Option<models::users::User>,
    pub flash: Option<FlashWrapper>,
}

#[derive(Template)]
#[template(path = "code.html")]
pub struct CodeTemplate {
    pub code: models::codes::Code,
    pub events: Vec<models::events::Event>,
    pub current_user: Option<models::users::User>,
    pub timelines: Vec<models::timelines::Timeline>,
    pub current_timeline: Option<models::timelines::Timeline>,
    pub flash: Option<FlashWrapper>,
}

#[derive(Template)]
#[template(path = "index.html")]
pub struct IndexTemplate {
    pub events: Vec<models::events::Event>,
    pub current_user: Option<models::users::User>,
    pub flash: Option<FlashWrapper>,
}

#[derive(Template)]
#[template(path = "login.html")]
pub struct LoginTemplate {
    pub current_user: Option<models::users::User>,
    pub flash: Option<FlashWrapper>,
}

#[derive(Template)]
#[template(path = "newuser.html")]
pub struct NewUserTemplate {
    pub current_user: Option<models::users::User>,
    pub flash: Option<FlashWrapper>,
}

#[derive(Template)]
#[template(path = "user.html")]
pub struct UserTemplate {
    pub current_user: Option<models::users::User>,
    pub focused_user: models::users::User,
    pub user_codes: Vec<models::codes::Code>,
    pub timelines: Vec<database::Result<models::timelines::TimelineSummary>>,
    pub flash: Option<FlashWrapper>,
}

#[derive(Template)]
#[template(path = "timeline.html")]
pub struct TimelineTemplate {
    pub current_user: Option<models::users::User>,
    pub timeline: models::timelines::TimelineSummary,
    pub codes: Vec<models::codes::Code>,
    pub events: Vec<models::events::Event>,
    pub flash: Option<FlashWrapper>,
}

#[derive(Template)]
#[template(path = "confirm.html")]
pub struct ConfirmTemplate {
    pub current_user: Option<models::users::User>,
    pub flash: Option<FlashWrapper>,
    pub question: String,
    pub yes_location: String,
    pub no_location: String,
}
