use rocket::request::FromForm;
use serde::Serialize;

#[derive(FromForm, Debug)]
pub struct LoginForm {
    pub email: String,
    pub password: String,
}

#[derive(FromForm, Debug, Serialize)]
pub struct NewUserForm {
    pub email: String,
    pub password: String,
}

#[derive(FromForm, Debug)]
pub struct TimelineForm {
    pub description: String,
}

#[derive(FromForm, Debug)]
pub struct CodeEditForm {
    pub description: String,
    pub timeline: String,
}
