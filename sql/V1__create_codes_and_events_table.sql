CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE users
(
    user_id  uuid PRIMARY KEY default uuid_generate_v4(),
    email    varchar NOT NULL UNIQUE,
    password varchar NOT NULL
);

CREATE TABLE timelines
(
    timeline_id uuid PRIMARY KEY default uuid_generate_v4(),
    description varchar NOT NULL,
    owner       uuid    NOT NULL REFERENCES users (user_id),
    deleted     bool             default false
);

CREATE TABLE codes
(
    code_id          uuid PRIMARY KEY                     default uuid_generate_v4(),
    description      varchar                     NOT NULL,
    created          timestamp without time zone NOT NULL default (now() at time zone 'utc'),
    user_id          uuid                        NOT NULL REFERENCES users (user_id),
    current_timeline uuid                        NOT NULL REFERENCES timelines (timeline_id)
);

CREATE TABLE events
(
    event_id    uuid PRIMARY KEY                     default uuid_generate_v4(),
    time        timestamp without time zone NOT NULL default (now() at time zone 'utc'),
    timeline_id uuid                        NOT NULL REFERENCES timelines (timeline_id),
    code_id     uuid                        NOT NULL REFERENCES codes (code_id)
);