mod common;
use select::document::Document;

extern crate qr_track;

use common::*;
use select::predicate::Class;
use surf;

/// Creates a new account and checks that:
///     * Account is logged in
///     * User page can be loaded
///     * New users have a single timeline and code created for them
#[async_std::test]
async fn create_new_account() {
    let mut resp = surf::post(uri("/newuser"))
        .body_form(&qr_track::forms::NewUserForm {
            email: random_email(),
            password: "test-password".to_string(),
        })
        .unwrap()
        .await
        .unwrap();

    // Assert redirected
    assert_eq!(resp.status(), 303);
    let redirect = resp.header("location").unwrap().to_string();

    // Check redirect is to user page. Would be to the /newuser if creation failed
    assert!(redirect.starts_with("/user/"));

    // get login cookie
    let cookie = get_user_id_cookie(&mut resp)
        .expect("Couldn't get login cookie");

    // check user page is loadable (requires login cookie)
    let mut resp = surf::get(uri(&redirect))
        .set_header("Cookie", cookie)
        .await
        .unwrap();
    let body = resp.body_string().await.unwrap();
    let user_page = Document::from(body.as_ref());

    // Check correct numbers of codes and timelines
    let timeline_count = user_page.find(Class("timeline-summary")).count();
    let code_count = user_page.find(Class("code-summary")).count();

    assert_eq!(timeline_count, 1);
    assert_eq!(code_count, 1);
}
