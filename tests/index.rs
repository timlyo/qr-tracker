pub mod common;

use select::predicate::Name;

#[async_std::test]
async fn test_index_responds() {
    let page = common::get_page("").await;

    let title = page
        .find(Name("title"))
        .next()
        .expect("Couldn't find title");

    assert_eq!(title.html(), "<title>Qr</title>")
}
