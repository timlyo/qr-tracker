use rand::{thread_rng, Rng};
use select::document::Document;
use surf::Response;

pub async fn get_page(route: &str) -> Document {
    let uri = format!("http://localhost:19000{}", route);
    eprintln!("Loading {}", uri);
    let mut resp = surf::get(uri).await.unwrap();

    assert_eq!(resp.status(), 200);

    let body = resp.body_string().await.unwrap();
    eprintln!("{}", body);
    Document::from(body.as_ref())
}

pub fn uri(route: &str) -> String {
    format!("http://localhost:19000/{}", route)
}

pub fn random_email() -> String {
    let id = thread_rng().gen::<u32>();
    format!("test-{}@test.com", id)
}

pub fn get_user_id_cookie(response: &mut Response) -> Option<String> {
    response
        .headers()
        .into_iter()
        .filter(|(key, value)| *key == "set-cookie" && value.contains("user_id"))
        .next()
        .map(|(_, cookie)| cookie.to_string())
}
